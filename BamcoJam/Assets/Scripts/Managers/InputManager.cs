using System;
using System.Collections.Generic;
using Constants;
using Interfaces;
using UnityEngine;

namespace Managers
{
    //TODO RENAME THISSSSS
    [RequireComponent(typeof(InputManager))]
    public class InputManager : MonoBehaviour
    {
        //TODO refactor
        //should this be a singleton?
        //no, should be attached to a controller
        //let's do that refactor and worry about multiplayer later

        public static InputManager Instance;

        private void Start()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
                return;
            }
            Instance = GetComponent<InputManager>();
            DontDestroyOnLoad(gameObject);
        }

        //Need for button down, button up, and button hold
        private Action<float, float> _lAxis;
        private Action<float, float> _rAxis;
        private Action<bool> _aButton;
        private Action<bool> _bButton;
        private Action<bool> _xButton;
        private Action<bool> _yButton;
        private Action<bool> _select;
        private Action<bool> _start;
        private Action<float> _shoulderL;
        private Action<float> _shoulderR;
        private Action<bool> _zLeft;
        private Action<bool> _zRight;
        private Action<char> _keyboardGeneric;
        //NEED PUSH L STICK AND PUSH R STICK
        //NEED DPAD
        //NOTE TEST OUT CHANGING JOYSTICK AXES TO JOYSTICK AXIS FROM KEY OR MOUSE BUTTON
        
        //stack of input things that never relinquished their input control request
        private readonly LinkedList<IHasInput> _requestedInputActions = new();

        public void TakeControl(IHasInput inputActions)
        {
            _requestedInputActions.AddFirst(inputActions);
            InitInput(inputActions.LAxis, inputActions.RAxis, inputActions.AButton, 
                inputActions.BButton, inputActions.XButton, inputActions.YButton, 
                inputActions.SelectButton, inputActions.StartButton, inputActions.ShoulderL, inputActions.ShoulderR,
                inputActions.ZLeft, inputActions.ZRight);
        }

        public void RelinquishInput(IHasInput inputActions)
        {
            if (!_requestedInputActions.Contains(inputActions)) return;
            _requestedInputActions.Remove(inputActions);
            UpdateInput();
        }

        private void UpdateInput()
        {
            IHasInput inputActions = _requestedInputActions.First.Value;
            InitInput(inputActions.LAxis, inputActions.RAxis, inputActions.AButton, 
                inputActions.BButton, inputActions.XButton, inputActions.YButton, 
                inputActions.SelectButton, inputActions.StartButton, inputActions.ShoulderL, inputActions.ShoulderR,
                inputActions.ZLeft, inputActions.ZRight);
        }

        private void InitInput(Action<float, float> lAxis, Action<float, float> rAxis, Action<bool> aButton, 
            Action<bool> bButton, Action<bool> xButton, Action<bool> yButton, Action<bool> select, 
            Action<bool> start, Action<float> shoulderL, Action<float> shoulderR, Action<bool> zLeft, 
            Action<bool> zRight, Action<char> keyboardGeneric = null)
        {
            _lAxis = lAxis ?? ((x,y) => {Debug.Log("Action not set");});
            _rAxis = rAxis ?? ((x,y) => {Debug.Log("Action not set");});
            _aButton = aButton ?? ((x) => {Debug.Log("Action not set");});
            _bButton = bButton ?? ((x) => {Debug.Log("Action not set");});
            _xButton = xButton ?? ((x) => {Debug.Log("Action not set");});
            _yButton = yButton ?? ((x) => {Debug.Log("Action not set");});
            _select = select ?? ((x) => {Debug.Log("Action not set");});
            _start = start ?? ((x) => {Debug.Log("Action not set");});
            _shoulderL = shoulderL ?? ((x) => {Debug.Log("Action not set");});
            _shoulderR = shoulderR ?? ((x) => {Debug.Log("Action not set");});
            _zLeft = zLeft ?? ((x) => {Debug.Log("Action not set");});
            _zRight = zRight ?? ((x) => {Debug.Log("Action not set");});
            _keyboardGeneric = keyboardGeneric ?? ((x) => {Debug.Log("Action not set");});
        }

        /*public void UpdateLAxisActions(Action<float, float> lAxis)
        {
            _lAxis = lAxis;
        }
        
        public void UpdateRAxisActions(Action<float, float> rAxis)
        {
            _rAxis = rAxis;
        }
        
        public void UpdateFaceButtonActions(Action aButton, Action bButton, Action xButton, Action yButton)
        {
            _aButton = aButton;
            _bButton = bButton;
            _xButton = xButton;
            _yButton = yButton;
        }
        
        public void UpdateCommandButtonActions(Action select, Action start)
        {
            _select = select;
            _start = start;
        }
        
        public void UpdateShoulderButtonActions(Action<float> shoulderL, Action<float> shoulderR, Action zLeft, 
            Action zRight)
        {
            _shoulderL = shoulderL;
            _shoulderR = shoulderR;
            _zLeft = zLeft;
            _zRight = zRight;
        }

        public void UpdateKeyboardGeneric(Action<char> keyboardGeneric = null)
        {
            _keyboardGeneric = keyboardGeneric ?? ((x) => { });
        }*/
         
        private void Update()
        {
            //if has keyboard
            UnityInputs();
            //if has controller (need different ones for different consoles)
            //ConsoleInputs();
        }

        private void UnityInputs()
        {
            var leftAxisHorizontal = Input.GetAxis(Keybinds.Axes.LeftStickHorizontal);
            var leftAxisVertical = Input.GetAxis(Keybinds.Axes.LeftStickVertical);
            if (Math.Abs(leftAxisHorizontal) >= 0.001 || Math.Abs(leftAxisVertical) >= 0.001)
            {
                _lAxis(leftAxisHorizontal, leftAxisVertical);
            }
            var rightAxisHorizontal = Input.GetAxis(Keybinds.Axes.RightStickHorizontal);
            var rightAxisVertical = Input.GetAxis(Keybinds.Axes.RightStickVertical);
            if (Math.Abs(rightAxisHorizontal) >= 0.001 || Math.Abs(rightAxisVertical) >= 0.001)
            {
                _rAxis(rightAxisHorizontal, rightAxisVertical);
            }
            
                    

            if (Input.GetButtonDown(Keybinds.Buttons.A)) _aButton(true);
            if (Input.GetButtonUp(Keybinds.Buttons.A)) _aButton(false);
            
            if (Input.GetButtonDown(Keybinds.Buttons.B)) _bButton(true);
            if (Input.GetButtonUp(Keybinds.Buttons.B)) _bButton(false);
            
            if (Input.GetButtonDown(Keybinds.Buttons.X)) _xButton(true);
            if (Input.GetButtonUp(Keybinds.Buttons.X)) _xButton(false);
            
            if (Input.GetButtonDown(Keybinds.Buttons.Y)) _yButton(true);
            if (Input.GetButtonUp(Keybinds.Buttons.Y)) _yButton(false);
            
            if (Input.GetButtonDown(Keybinds.Buttons.Select)) _select(true);
            if (Input.GetButtonUp(Keybinds.Buttons.Select)) _select(false);
            
            if (Input.GetButtonDown(Keybinds.Buttons.Start)) _start(true);
            if (Input.GetButtonUp(Keybinds.Buttons.Start)) _start(false);
            
            if (Input.GetButtonDown(Keybinds.Buttons.ZL)) _zLeft(true);
            if (Input.GetButtonUp(Keybinds.Buttons.ZL)) _zLeft(false);
            
            if (Input.GetButtonDown(Keybinds.Buttons.ZR)) _zRight(true);
            if (Input.GetButtonUp(Keybinds.Buttons.ZR)) _zRight(false);
            
            
            //Should I make this act like a button?
            var leftShoulder = Input.GetAxis(Keybinds.Axes.LeftShoulder);
            if (Math.Abs(leftShoulder) >= 0.001)
            {
                _shoulderL(leftShoulder);
            }
            var rightShoulder = Input.GetAxis(Keybinds.Axes.RightShoulder);
            if (Math.Abs(rightShoulder) >= 0.001)
            {
                _shoulderR(rightShoulder);
            }
        }

        private void DefaultAxis(float a, float b)
        {
            
        }

        private void DefaultButton()
        {
            
        }

        private void DefaultShoulder(float a)
        {
            
        }
    }
}
