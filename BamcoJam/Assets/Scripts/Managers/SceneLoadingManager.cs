using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Managers
{
    public class SceneLoadingManager : MonoBehaviour
    {
        public static SceneLoadingManager instance;

        private void Awake()
        {
            if (instance != null && instance != this)
            {
                Destroy(gameObject);
                return;
            }

            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        //TODO also do loading art, etc...
        public void LoadScene(string sceneName)
        {
            StartCoroutine(LoadSceneCoroutine(sceneName));
        }

        IEnumerator LoadSceneCoroutine(string sceneName)
        {
            AsyncOperation loadingScene = SceneManager.LoadSceneAsync(sceneName);

            while (!loadingScene.isDone)
            {
                Debug.Log(loadingScene.progress);
                yield return null;
            }
        }

        //TODO Load scene but wait for button press to continue
    }
}
