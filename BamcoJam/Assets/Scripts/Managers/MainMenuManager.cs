using Constants;
using UnityEngine;
using Button = UnityEngine.UI.Button;

namespace Managers
{
    public class MainMenuManager : MonoBehaviour
    {
        //TODO change to using some menu item that you can select with controller/hover to select
        [SerializeField] public Button playButton;
        
        // Start is called before the first frame update
        void Start()
        {
            playButton.onClick.AddListener(LoadGame);
        }

        //TODO should be checking what scene to load, loading player data (?), etc...
        private void LoadGame()
        {
            SceneLoadingManager.instance.LoadScene(Scenes.GameScene);
        }
    }
}
