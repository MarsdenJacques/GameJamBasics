using System;
using Constants;
using UnityEngine;

namespace Managers
{
    public class BootstrapManager : MonoBehaviour
    {
        private void Start()
        {
            SceneLoadingManager.instance.LoadScene(Scenes.MainMenu);
        }
    }
}