using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Managers
{
    public class MenuManager : MonoBehaviour
    {
        public static MenuManager Instance;

        public bool currentSelectionHasCycleOptions = false;
        public bool canPop = false;
        
        //public UI big
        //public UI mid
        //public UI small
        //public UI Item text
            //should have a selected and non-selected state
        //public UI Item item
            //should have a selected and non-selected state

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        
        //ability to have things which require a menu to send it a menu to display, then activates
        //on activation menu input controller requests control?
        //main menu (active) > character runs about (inactive) > returns to menu
        //only really weird with persistent menus that aren't the result of interacting with stuff
        //this shouldn't be an issue?
        //if it is, just keep the input controller active but disable the menu UI
        //the new thing will take over control, then relinquish it back
        //Should there be a stack?
        //Menu object with menu tree in it
    }
}