namespace Constants
{
    public static class Keybinds
    {
        public static class Axes
        {
            public static string LeftStickHorizontal = "HorizontalL";
            public static string LeftStickVertical = "VerticalL";
            public static string RightStickHorizontal = "HorizontalR";
            public static string RightStickVertical = "VerticalR";
            public static string LeftShoulder = "ShoulderL";
            public static string RightShoulder = "ShoulderR";
        }

        public static class Buttons
        {
            public static string A = "A";
            public static string B = "B";
            public static string X = "X";
            public static string Y = "Y";
            public static string ZL = "ZL";
            public static string ZR = "ZR";
            public static string Select = "Select";
            public static string Start = "Start";
        }
    }
}