using UnityEngine;

namespace Constants
{
    public static class Scenes
    {
        public const string Bootstrap = "Bootstrap";
        public const string MainMenu = "MainMenu";
        //TODO there should be multiple game scenes for different areas
        public const string GameScene = "GameScene";
    }
}