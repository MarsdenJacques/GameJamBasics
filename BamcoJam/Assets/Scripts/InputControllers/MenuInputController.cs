using System;
using Interfaces;
using Managers;
using UnityEngine;

namespace InputControllers
{
    public class MenuInputController : MonoBehaviour, IHasInput
    {
        private MenuManager _menuManager;
        
        private void Awake()
        {
            InitActions();
            _menuManager = MenuManager.Instance;
        }

        public void InitActions()
        {
            //set all the actions here
            LAxis = Navigate;
            AButton = Select;
            BButton = PopMenuTree;
            SelectButton = Select;
            StartButton = CloseMenu;
        }

        private void Navigate(float horizontal, float vertical)
        {
            if (_menuManager.currentSelectionHasCycleOptions)
            {
                if (Math.Abs(horizontal) > Math.Abs(vertical))
                {
                    //check for direction
                    //cycle option
                    Debug.Log("Cycle option");
                }
            }
            
            if (Math.Abs(vertical) >= 0.001)
            {
                //check for direction
                Debug.Log("Change selection");
            }
        }

        private void Select(bool buttonDown)
        {
            if (!buttonDown) return;
            //menu manager select current
        }

        private void PopMenuTree(bool buttonDown)
        {
            if (!buttonDown) return;
            if (_menuManager.canPop)
            {
                Debug.Log("Pop"); //pop
                return;
            }
            //if cannot pop, CloseMenu
            CloseMenu(true);
        }

        private void PushMenuTree()
        {
            //if currently highlighted has sub tree, push
        }

        private void CloseMenu(bool buttonDown)
        {
            if (!buttonDown) return;
            //close menu
            Debug.Log("Close menu");
        }

        public void TakeControl()
        {
            InputManager.Instance.TakeControl(this);
        }

        public void RelinquishControl()
        {
            InputManager.Instance.RelinquishInput(this);
        }

        private void OnDestroy()
        {
            //Relinquish control before destroying
            //RelinquishControl();
        }

        public Action<float, float> LAxis { get; set; }
        public Action<float, float> RAxis { get; set; }
        public Action<bool> AButton { get; set; }
        public Action<bool> BButton { get; set; }
        public Action<bool> XButton { get; set; }
        public Action<bool> YButton { get; set; }
        public Action<bool> SelectButton { get; set; }
        public Action<bool> StartButton { get; set; }
        public Action<float> ShoulderL { get; set; }
        public Action<float> ShoulderR { get; set; }
        public Action<bool> ZLeft { get; set; }
        public Action<bool> ZRight { get; set; }
        public Action<char> KeyboardGeneric { get; set; }
    }
}