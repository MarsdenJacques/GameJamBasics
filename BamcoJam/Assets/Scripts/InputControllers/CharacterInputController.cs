using System;
using Interfaces;
using Managers;
using UnityEngine;

namespace InputControllers
{
    public class CharacterInputController : MonoBehaviour, IHasInput
    {
        private bool _running = false;
        //Handle multiple in the same area
        //Set OnTriggerEnter
        private ISelectable _currentSelectable = null;
        
        private void Start()
        {
            InitActions();
            TakeControl();
        }

        public void InitActions()
        {
            LAxis = Movement;
            RAxis = (a, b) => { };
            AButton = InteractWithSelectable;
            BButton = Run;
            XButton = (a) => { }; //idk yet
            YButton = Inventory;
            SelectButton = (a) => { };
            StartButton = Pause;
        }

        private void Movement(float horizontal, float vertical)
        {
            gameObject.transform.Translate(new Vector3(horizontal, vertical, 0) * Time.deltaTime);
        }

        private void InteractWithSelectable(bool buttonDown)
        {
            //Should select pass the selector?
            //I don't think so
            if (buttonDown)
            {
                _currentSelectable.Select();
            }
        }

        //How to handle hold?
        private void Run(bool buttonDown)
        {
            _running = buttonDown;
        }

        private void Inventory(bool buttonDown)
        {
            //open inventory
        }
        
        //Pause as singleton?
        //Also want to pause on inventory open
        private void Pause(bool buttonDown)
        {
            
        }

        public void TakeControl()
        {
            InputManager.Instance.TakeControl(this);
        }

        public void RelinquishControl()
        {
            InputManager.Instance.RelinquishInput(this);
        }

        private void OnDestroy()
        {
            //Relinquish control before destroying
            //RelinquishControl();
        }

        public Action<float, float> LAxis { get; set; }
        public Action<float, float> RAxis { get; set; }
        public Action<bool> AButton { get; set; }
        public Action<bool> BButton { get; set; }
        public Action<bool> XButton { get; set; }
        public Action<bool> YButton { get; set; }
        public Action<bool> SelectButton { get; set; }
        public Action<bool> StartButton { get; set; }
        public Action<float> ShoulderL { get; set; }
        public Action<float> ShoulderR { get; set; }
        public Action<bool> ZLeft { get; set; }
        public Action<bool> ZRight { get; set; }
        public Action<char> KeyboardGeneric { get; set; }
    }
}