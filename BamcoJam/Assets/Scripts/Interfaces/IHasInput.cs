using System;
using Managers;

namespace Interfaces
{
    public interface IHasInput
    {
        Action<float, float> LAxis { get; set; }
        Action<float, float> RAxis { get; set; }
        Action<bool> AButton { get; set; }
        Action<bool> BButton { get; set; }
        Action<bool> XButton { get; set; }
        Action<bool> YButton { get; set; }
        Action<bool> SelectButton { get; set; }
        Action<bool> StartButton { get; set; }
        Action<float> ShoulderL { get; set; }
        Action<float> ShoulderR { get; set; }
        Action<bool> ZLeft { get; set; }
        Action<bool> ZRight { get; set; }
        //NEED PUSH L STICK AND PUSH R STICK
        //NEED DPAD
        Action<char> KeyboardGeneric { get; set; }

        public void InitActions();
        public void TakeControl();
        public void RelinquishControl();
    }
}