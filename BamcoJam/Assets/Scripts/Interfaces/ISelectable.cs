namespace Interfaces
{
    public interface ISelectable
    {
        //action for what to do when selected
        void Select();
    }
}